<?php
/**
 * @file
 * Contains the administrative page and form callbacks for the Weight Rate module.
 */

/**
 * Form builder of weight rate service
 */
function commerce_weight_rate_service_form($form, &$form_state, $shipping_service) {
  // Store the initial shipping service in the form state.
  $form_state['shipping_service'] = $shipping_service;

  $form['service'] = array(
    '#tree' => TRUE,
  );

  $form['service']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $shipping_service['title'],
    '#description' => t('The administrative title of this weight rate. It is recommended that this title begin with a capital letter and contain only letters, numbers, and spaces.'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  if (!empty($shipping_service['name'])) {
    $form['service']['title']['#field_suffix'] = ' <small id="edit-weight-rate-title-suffix">' . t('Machine name: @name', array('@name' => $shipping_service['name'])) . '</small>';
  }

  if (empty($shipping_service['name'])) {
    $form['service']['name'] = array(
      '#type' => 'machine_name',
      '#default_value' => $shipping_service['name'],
      '#maxlength' => 32,
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'commerce_shipping_service_load',
        'source' => array('service', 'title'),
      ),
      '#description' => t('The machine-name of this weight rate. This name must contain only lowercase letters, numbers, and underscores. It must be unique.'),
    );
  }
  else {
    $form['service']['name'] = array(
      '#type' => 'value',
      '#value' => $shipping_service['name'],
    );
  }

  $form['service']['shipping_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping address'),
    '#default_value' => $shipping_service['shipping_address'],
    '#description' => t('The shipping address of this weight rate service belongs to. this field is not necessary'),
    '#size' => 32,
  );

  $form['service']['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display title'),
    '#default_value' => $shipping_service['display_title'],
    '#description' => t('The front end display title of this weight rate shown to customers. Leave blank to default to the <em>Title</em> from above.'),
    '#size' => 32,
  );

  $form['service']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Describe this weight rate if necessary. The text will be displayed in the weight rate services overview table.'),
    '#default_value' => $shipping_service['description'],
    '#rows' => 3,
  );

  $form['actions'] = array(
    '#type' => 'container',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save weight rate'),
  );

  if (!empty($form_state['shipping_service']['name'])) {
    $destination_areas = db_query('SELECT * FROM {commerce_weight_rate_destination_area} WHERE service_name = !service_name', array('!service_name' => $form_state['shipping_service']['name']));

    // if service is not new display the delete button.
    $form['actions']['add_destination_area'] = array(
      '#type' => 'submit',
      '#value' => t('Destination area manage'),
      '#submit' => array('commerce_weight_rate_service_form_goto_destination_area_submit'),
      '#weight' => 40,
    );

    // if service is not new display the delete button.
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete weight rate'),
      '#suffix' => l(t('Cancel'), 'admin/commerce/config/shipping/services/weight-rate'),
      '#submit' => array('commerce_weight_rate_service_form_delete_submit'),
      '#weight' => 45,
    );
  }
  else {
    $form['actions']['submit']['#suffix'] = l(t('Cancel'), 'admin/commerce/config/shipping/services/weight-rate');
  }
  return $form;
}

/**
 * Validate handler: validate is not necessary now.
 */
function commerce_weight_rate_service_form_validate($form, &$form_state) {
}

/**
 * Submit handler: preprocess service data and invoke save.
 */
function commerce_weight_rate_service_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  // Update the shipping service array with values from the form.
  foreach (array('name', 'title', 'shipping_address', 'display_title', 'description') as $key) {
    $shipping_service[$key] = $form_state['values']['service'][$key];
  }

  $op = commerce_weight_rate_service_save($shipping_service);

  if (!$op) {
    drupal_set_message(t('The weight rate service failed to save properly. Please review the form and try again.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  else {
    drupal_set_message(t('Weight rate service saved.'));
    $form_state['redirect'] = 'admin/commerce/config/shipping/services/weight-rate';
  }
}

/**
 * Submit handler: redirects to the weight rate service delete confirmation form.
 */
function commerce_weight_rate_service_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/config/shipping/services/weight-rate-' . strtr($form_state['shipping_service']['name'], '_', '-') . '/delete';
}

/**
 * Displays the edit form for an existing weight rate service.
 *
 * @param $name
 *   The machine-name of the weight rate service to edit.
 */
function commerce_weight_rate_service_edit_page($name) {
  return drupal_get_form('commerce_weight_rate_service_form', commerce_shipping_service_load($name));
}

/**
 * Builds the form for deleting weight rate services.
 */
function commerce_weight_rate_service_delete_form($form, &$form_state, $shipping_service) {
  $form_state['shipping_service'] = $shipping_service;

  $form = confirm_form($form,
    t('Are you sure you want to delete the <em>%title</em> weight rate service?', array('%title' => $shipping_service['title'])),
    'admin/commerce/config/shipping/services/weight-rate',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_weight_weight_service_delete_form().
 */
function commerce_weight_rate_service_delete_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  commerce_weight_rate_service_delete($shipping_service['name']);

  drupal_set_message(t('The weight rate service <em>%title</em> has been deleted.', array('%title' => $shipping_service['title'])));
  watchdog('commerce_weight_rate', 'Deleted weight rate service <em>%title</em>.', array('%title' => $shipping_service['title']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/commerce/config/shipping/services/weight-rate';
}

/**
 * Displays the delete confirmation form for an existing flat rate service.
 *
 * @param $name
 *   The machine-name of the flat rate service to delete.
 */
function commerce_weight_rate_service_delete_page($name) {
  return drupal_get_form('commerce_weight_rate_service_delete_form', commerce_shipping_service_load($name));
}

/**
 * Submit callback for add destination area submit
 */
function commerce_weight_rate_service_form_goto_destination_area_submit($form, &$form_state) {
  $service_name_arg = 'weight-rate-' . strtr($form_state['shipping_service']['name'], '_', '-');
  $form_state['redirect'] = 'admin/commerce/config/shipping/services/' . $service_name_arg . '/destination-area';
}

/**
 * Displays the edit form for an existing destination area.
 *
 * @param $name
 *   The machine-name of the weight rate service to edit.
 */
function commerce_weight_rate_service_destination_area_edit_page($name, $da_id) {
  return drupal_get_form('commerce_weight_rate_service_destination_area_form', $name, commerce_shipping_service_destination_area_load($da_id));
}

/**
 * delete form for an existing destination area.
 *
 * @param $name
 *   The machine-name of the weight rate service to edit.
 */
function commerce_weight_rate_service_destination_area_delete_page($name, $da_id) {
  return drupal_get_form('commerce_weight_rate_service_destination_area_delete_form', commerce_shipping_service_destination_area_load($da_id));
}

/**
 * Form builder for destination area delete page.
 */
function commerce_weight_rate_service_destination_area_delete_form($form, &$form_state, $destination_area) {
  $form_state['destination_area'] = $destination_area;

  $form = confirm_form($form,
    t('Are you sure you want to delete the <em>%name</em> destination area?', array('%name' => $destination_area['name'])),
    'admin/commerce/config/shipping/services/weight-rate',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit handler: commerce_weight_rate_service_destination_area_delete_form()
 */
function commerce_weight_rate_service_destination_area_delete_form_submit($form, &$form_state) {
  $destination_area = $form_state['destination_area'];

  commerce_weight_rate_destination_area_delete($destination_area['da_id']);

  drupal_set_message(t('The destination area <em>%name</em> has been deleted.', array('%name' => $destination_area['name'])));
  watchdog('commerce_weight_rate', 'Deleted weight rate service <em>%name</em>.', array('%name' => $destination_area['name']), WATCHDOG_NOTICE);
}

/**
 * Destination area form builder
 */
function commerce_weight_rate_service_destination_area_form($form, &$form_state, $name, $destination_area) {
  $form_state['shipping_service'] = commerce_shipping_service_load($name);
  $form_state['destination_area'] = $destination_area;

  // weight tariff list init
  if (!isset($form_state['weight_tariff_list'])) {
    $form_state['weight_tariff_list'] = array();
    if (!empty($destination_area['da_id'])) {
      $query = db_select('commerce_weight_rate_tariff', 't')->fields('t');
      $results = $query->condition('da_id', $destination_area['da_id'])->execute();
      while($record = $results->fetchAssoc()) {
        $record['amount'] = commerce_currency_amount_to_decimal($record['amount'], $record['currency_code']);
        $form_state['weight_tariff_list'][$record['tariff_id']] = $record;
      }
    }
  }
  uasort($form_state['weight_tariff_list'], '_commerce_weight_rate_weight_tariff_sort_compare_callback');

  $form['destination_area'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['destination_area']['service_name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );

  $form['destination_area']['name'] = array(
    '#type' => 'textfield',
    '#default_value' => $destination_area['name'],
    '#title' => t('Name'),
    '#description' => t('The name of destination area'),
    '#required' => TRUE,
  );

  commerce_weight_rate_attach_weight_element($form['destination_area'], $destination_area['weight'], $destination_area['unit']);
  commerce_weight_rate_attach_price_element($form['destination_area'], $destination_area['amount'], $destination_area['currency_code']);

  // update fill destinations
  $destinations = array();
  if (!empty($destination_area['da_id'])) {
    $destinations = $query = db_select('commerce_weight_rate_destination', 'd')
      ->fields('d', array('name'))
      ->condition('da_id', $destination_area['da_id'])
      ->execute()
      ->fetchCol();
  }

  $form['destinations'] = array(
    '#type' => 'textarea',
    '#title' => t('Destination List'),
    '#description' => t('The addresses be included by current destination area, For comparisons with multiple possible values, place separate values on new lines.'),
    '#default_value' => implode("\n", $destinations),
    '#required' => TRUE,
  );

  // weight tariff section
  $form['weight_tariff'] = array(
    '#type' => 'fieldset',
    '#title' => t('Weight tariff'),
    '#tree' => TRUE,
  );
  $form['weight_tariff']['#attached']['css'][] = drupal_get_path('module', 'commerce_weight_rate') . '/commerce_weight_rate.admin.css';

  $form['weight_tariff']['weight_tariff_container'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="weight-tariff-container">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  $weight_tariff_options = array();

  foreach ($form_state['weight_tariff_list'] as $key => $weight_tariff) {
    $weight_tariff_options[$key] = theme('commerce_weight_rate_weight_tariff_item', array('element' => $weight_tariff));
  }
  $form['weight_tariff']['weight_tariff_container']['weight_tariff_items'] = array(
    '#type' => 'checkboxes',
    '#options' => $weight_tariff_options,
  );

  // weight tariff form items
  $form['weight_tariff']['symbol'] = array(
    '#type' => 'select',
    '#title' => t('Symbol'),
    '#prefix' => '<div class="tariff-row clearfix">',
    '#required' => TRUE,
    '#options' => commerce_weight_rate_compare_symbols(),
    '#default_value' => '1',
  );
  // charge mode select
  $charge_modes = commerce_weight_rate_charge_modes();

  $charge_mode_options = array();
  foreach ($charge_modes as $name => $charge_mode) {
    $charge_mode_options[$name] = $charge_mode['label'];
  }

  $form['weight_tariff']['charge_mode'] = array(
    '#type' => 'select',
    '#title' => t('Charge mode'),
    '#required' => TRUE,
    '#options' => $charge_mode_options,
  );

  commerce_weight_rate_attach_weight_element($form['weight_tariff'], 1);
  commerce_weight_rate_attach_price_element($form['weight_tariff']);

  $form['weight_tariff']['currency_code']['#suffix'] = '</div> <!-- /.tariff-row -->'; // close tag for "tariff-row"
  $form['weight_tariff']['save_tariff'] = array(
    '#type' => 'submit',
    '#value' => t('Save tariff'),
    '#limit_validation_errors' => array(
      array('weight_tariff', 'symbol'),
      array('weight_tariff', 'charge_mode'),
      array('weight_tariff', 'amount'),
      array('weight_tariff', 'currency_code'),
      array('weight_tariff', 'weight'),
      array('weight_tariff', 'unit'),
    ),
    '#ajax' => array(
      'callback' => 'commerce_weight_rate_weight_tariff_ajax_callback',
      'wrapper' => 'weight-tariff-container',
      'event' => 'click',
    ),
    '#validate' => array('commerce_weight_rate_weight_tariff_validate'),
    '#submit' => array('commerce_weight_rate_weight_tariff_submit'),
  );

  $form['weight_tariff']['delete_tariff'] = array(
    '#type' => 'submit',
    '#value' => t('Delete tariff'),
    '#limit_validation_errors' => array(
      array('weight_tariff', 'weight_tariff_container', 'weight_tariff_items'),
    ),
    '#ajax' => array(
      'callback' => 'commerce_weight_rate_weight_tariff_ajax_callback',
      'wrapper' => 'weight-tariff-container',
      'event' => 'click',
    ),
    '#submit' => array('commerce_weight_rate_weight_tariff_delete_submit'),
  );

  // actions wrapper
  $form['actions'] = array(
    '#type' => 'container',
  );

  $limit_validation_errors = array();
  foreach(array_keys($form['destination_area']) as $key) {
    $limit_validation_errors[] = array('destination_area', $key);
  }
  $limit_validation_errors[] = array('destinations');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#limit_validation_errors' => $limit_validation_errors,
    '#submit' => array('commerce_weight_rate_service_destination_area_save_submit'),
  );

  return $form;
}

/**
 * Ajax callback of weight tariff submited
 */
function commerce_weight_rate_weight_tariff_ajax_callback($form, $form_state) {
  return $form['weight_tariff']['weight_tariff_container'];
}

/**
 * Submit handler of destination area form.
 */
function commerce_weight_rate_service_destination_area_save_submit($form, &$form_state) {
  // save the destination area inforamtion.
  $destination_area = $form_state['destination_area'];
  $destinations = $form_state['values']['destinations'];
  $weight_tariff_list = $form_state['weight_tariff_list'];

  // Update the shipping service array with values from the form.
  foreach (array('name', 'weight', 'unit', 'amount', 'currency_code', 'service_name') as $key) {
    $destination_area[$key] = $form_state['values']['destination_area'][$key];
  }

  $destinations = preg_split('/[\n\r]+/', $destinations);
  $result = commerce_weight_rate_destination_area_save($destination_area, $destinations, $weight_tariff_list);
  if ($result === FALSE) {
    drupal_set_message(t('An unknown error occurs'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  elseif ($result === SAVED_NEW) {
    drupal_set_message(t('New destination area created'));

    $service_name_arg = 'weight-rate-' . strtr($form_state['shipping_service']['name'], '_', '-');
    $form_state['redirect'] = 'admin/commerce/config/shipping/services/' . $service_name_arg . '/destination-area';
  }
  else {
    drupal_set_message(t('Destination area update successed'));
  }
}

/**
 * Validate handler of weight tariff row save.
 */
function commerce_weight_rate_weight_tariff_validate($form, &$form_state) {
}

/**
 * Submit hanlder of weight tariff row save.
 */
function commerce_weight_rate_weight_tariff_submit($form, &$form_state) {
  $form_state['weight_tariff_list'][] = $form_state['values']['weight_tariff'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler of weight tariff delete.
 */
function commerce_weight_rate_weight_tariff_delete_submit($form, &$form_state) {
  $items = $form_state['values']['weight_tariff']['weight_tariff_container']['weight_tariff_items'];

  foreach ($items as $key => $value) {
    if (!empty($value) || is_string($value)) {
      unset($form_state['weight_tariff_list'][$value]);
    }
  }

  $form_state['rebuild'] = TRUE;
}

function _commerce_weight_rate_weight_tariff_sort_compare_callback($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/**
 * Weight element buidler
 */
function commerce_weight_rate_attach_weight_element(&$form, $weight = '', $unit = 'kg') {
  // Add a textfield for the actual weight value.
  $form['weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#default_value' => $weight,
    '#size' => 15,
    '#maxlength' => 16,
    '#required' => TRUE,
  );

  $options = physical_weight_unit_options(FALSE);
  $form['#attached']['css'][] = drupal_get_path('module', 'physical') . '/theme/physical.css';
  $form['weight']['#prefix'] = '<div class="physical-weight-textfield">';

  $form['unit'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $unit,
    '#suffix' => '</div>',
  );
}

/**
 * Price element builder
 */
function commerce_weight_rate_attach_price_element(&$form, $amount = '', $currency_code = 'USD') {
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Base rate'),
    '#default_value' => commerce_currency_amount_to_decimal($amount, $currency_code),
    '#required' => TRUE,
    '#size' => 10,
    '#prefix' => '<div class="commerce-flat-rate-base-rate">',
  );

  // Build a currency options list from all enabled currencies.
  $options = commerce_currency_get_code(TRUE);

  // If the current currency value is not available, add it now with a
  // message in the help text explaining it.
  if (!empty($options[$currency_code])) {
    $options[$currency_code] = check_plain($options[$currency_code]);

    $description = t('The currency set for this rate is not currently enabled. If you change it now, you will not be able to set it back.');
  }
  else {
    $description = '';
  }

  if (count($options) == 1) {
    $currency_code = key($options);

    $form['amount']['#field_suffix'] = check_plain($currency_code);
    $form['amount']['#suffix'] = '</div>';

    $form['currency_code'] = array(
      '#type' => 'value',
      '#default_value' => $currency_code,
    );
  }
  else {
    $form['currency_code'] = array(
      '#type' => 'select',
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $currency_code,
      '#suffix' => '</div>',
    );
  }
}

/**
 * Menu page callback of destination area list page
 */
function commerce_weight_rate_service_destination_area_page($name) {
  $output = array();

  $query = db_select('commerce_weight_rate_destination_area', 'da')->extend('TableSort');
  $destination = drupal_get_destination();
  $service_name_arg = 'weight-rate-' . strtr($name, '_', '-');

  // Build the table header.
  $header = array(
    'name' => array('data' => t('Name'), 'field' => 'da.name'),
    'weight' => array('data' => t('Weight'), 'field' => 'da.weight'),
    'price' => array('data' => t('Base Price'), 'field' => 'da.amount'),
    'operations' => array('data' => t('Operations')),
  );

  $results = $query->fields('da')
    ->condition('service_name', $name)
    ->orderByHeader($header)
    ->execute();

  $rows = array();
  while($record = $results->fetchObject()) {
    $rows[$record->da_id] = array(
      'name' => $record->name,
      'weight' => $record->weight . $record->unit,
      'price' => commerce_currency_amount_to_decimal($record->amount, $record->currency_code) . $record->currency_code,
    );

    $operations = array();

    $operations['edit'] = array(
      'title' => t('Edit'),
      'href' => 'admin/commerce/config/shipping/services/' . $service_name_arg . '/destination-area/' . $record->da_id . '/edit',
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('Delete'),
      'href' => 'admin/commerce/config/shipping/services/' . $service_name_arg . '/destination-area/' . $record->da_id . '/delete',
      'query' => $destination,
    );
    $rows[$record->da_id]['operations'] = array(
      'data' => array(
        '#theme' => 'links__destination_area_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }

  $output['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No content available.'),
  );

  return $output;
}
